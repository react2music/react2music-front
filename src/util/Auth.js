import axios from "axios";
import {R2M_API_LOGIN_URL} from "./config";
import jwtDecode from 'jwt-decode';
import {toast} from "react-toastify";

function authenticate(loginData) {
    return axios
        .post(R2M_API_LOGIN_URL, loginData)
        .then(response => response.data.token)
        .then(token => {
            console.log(token);
            window.localStorage.setItem("authToken", token);
            setToken(token);
        });
}

function logout() {

    window.localStorage.removeItem("authToken");
    delete axios.defaults.headers["Authorization"];
    toast.configure()
    toast.success("Bye bye, come back soon.", {
        position: toast.POSITION.BOTTOM_RIGHT,
        className: 'Pink'
    });
}

//checking for a valid token
function checkToken(){
    const token = window.localStorage.getItem("authToken");
    //if there is a token saved, decoding it to check the expiring date
    if(token){
        const jwtData = jwtDecode(token);
        console.log("There is a saved token that contains:", jwtData);
        //if expiring date is somewhere in the future (bigger that current date)
        const d = new Date(0);
        d.setUTCSeconds(jwtData.exp)
        console.log("It will expire on: ", d);
        if (jwtData.exp > new Date().getTime()/1000){
            //still a valid token, will use it
            setToken(token);
        }
    }else {
        console.log("No JWT token saved.");
    }
}

function setToken(token) {
    axios.defaults.headers["Authorization"]= "Bearer " + token;
}

function isAuthenticated() {
    const token = window.localStorage.getItem("authToken");
    //if there is a token saved, decoding it to check the expiring date
    if(token) {
        const jwtData = jwtDecode(token);
        //if expiring date is somewhere in the future (bigger that current date)
        if (jwtData.exp > new Date().getTime() / 1000) {
            //still a valid token = user connected
            console.log("Valid token.");
            return true;
        } else {
        console.log("Expired token.");
        return false; //token expired
        }
    }
    console.log("No token.");
    return false; //there is no token
}

export default {
    authenticate,
    logout,
    checkToken,
    isAuthenticated
};
