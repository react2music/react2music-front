import axios from "axios";
import {R2M_API_USERS_URL} from "./config";

function register(user) {
     return axios.post(R2M_API_USERS_URL, user);
}

export default {
    register
};