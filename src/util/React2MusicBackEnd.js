import {R2M_URL_SEARCH} from "./config";

const url = 'http://localhost:8000'; //8181
const React2MusicBackEnd = {

    search(term) {
        console.log(`${url}/api/${term}.json`);
        //`http://localhost:8181/api/artists/${term}.json`  //will do when having the search from the backend implemented
        if (term === "tracks") {

            return fetch(`${url}/api/${term}.json`
            ).then(response => {
                return response.json();
            }).then(jsonResponse => {
                if (!jsonResponse) { //no results in the response
                    return [];
                }
                console.log("returning response");
                console.log(jsonResponse);
                return jsonResponse.map(track => ({
                    id: track.id,
                    name: track.name,
                    duration: track.duration,
                    bpm: track.bpm,
                    album: track.album.name,
                    fileMp3: `http://localhost:8000/uploads/tracks/${track.file_mp3}`,
                    artist: track.artist.name
                }));
            })
        }//if term == "tracks"
        else { //will search for the 'term' in artists/tracks/albums and will return all matches
            return fetch(`${R2M_URL_SEARCH}${term}`
            ).then(response => {
                return response.json();
            }).then(jsonResponse => {
                if (!jsonResponse) { //no results in the response
                    return [];
                }
                console.log("returning response");
                console.log(jsonResponse);
                return jsonResponse.tracks.map(track => ({
                    id: track.id,
                    name: track.name,
                    duration: track.duration,
                    bpm: track.bpm,
                    album: track.album.name,
                    cover: track.album.cover,
                    fileMp3: `http://localhost:8000/uploads/tracks/${track.file_mp3}`,
                    artist: track.artist.name
                }));
            })
        }
    }
};

export default React2MusicBackEnd;