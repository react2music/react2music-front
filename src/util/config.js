export const R2M_API_URL = 'http://localhost:8181/api';
export const R2M_URL = 'http://localhost:8181/uploads';
export const R2M_API_LOGIN_URL = R2M_API_URL + '/login_check';
export const R2M_API_USERS_URL = R2M_API_URL + '/users';

export const R2M_URL_SEARCH = R2M_API_URL + '/search/';

export const  R2M_URL_ARTISTS_POSTERS =  R2M_URL + '/artist_poster/';

export const R2M_URL_TRACKS = 'http://localhost:8000/public/uploads/tracks/';
