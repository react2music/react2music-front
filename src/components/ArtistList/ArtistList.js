import React from "react";
import axios from "axios";
import "./ArtistList.css";
import Artist from "../Artist/Artist";
import Track from "../Track/Track";



class ArtistList extends React.Component{
          state={
            artists:[]

        };
            componentDidMount() {

          axios
              .get("http://localhost:8181/api/artists?page=1").then((response)=> {
             // console.log(response.data['hydra:member']);
              let artistsResponse = response.data['hydra:member'];
              console.log(artistsResponse);
              console.log(artistsResponse[0]);
              console.log(artistsResponse[0].name);
              console.log(response)

              this.setState(
                  {artists: artistsResponse},
                  ()=> {console.log(this.state.artists[0])});
          });
            };

     render(){
        return(
         <div className="container">
             {
             this.state.artists.map((artist) => {
             return <Artist artistProp={artist}
             key={artist.id}
             />
             })
             }
         </div>
        )
   }

}

export default ArtistList;