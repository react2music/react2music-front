import React from 'react';
import "./Artist.css";
import { R2M_URL_ARTISTS_POSTERS } from '../../util/config';


const Artist= (props) =>{
    const url = `${R2M_URL_ARTISTS_POSTERS}${props.artistProp.poster}`;
    return(
        <div className="container">
            <img src={url} alt=""/>
            <li> {props.artistProp.name} </li>
            <li> {console.log(props.artistProp)} </li>

        </div>
    );

}

export default Artist;