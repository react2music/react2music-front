import React from 'react';
import './App.css';
import Search from "../../pages/Search";
import Login from "../../pages/Login";
import Register from "../../pages/Register";
import NavBar from '../NavBar/NavBar';
import ArtistList from "../ArtistList/ArtistList";

//import Carousel from '../Carousel/Carousel';
import 'react-toastify/dist/ReactToastify.css';
import Auth from '../../util/Auth';


import { BrowserRouter as Router,
    Switch,
    Route,
    withRouter
}
    from "react-router-dom";

class App extends React.Component {
    constructor(props) {
        super(props);
        /*
        isAuthenticated will be used to display or not different elements on the page
        (login - if user anomymous, log out - if user is connected)
         */
        this.state = {
            isAuthenticated: false
        }
        this.setIsAuthenticated = this.setIsAuthenticated.bind(this);
    }
    setIsAuthenticated(){
        this.setState({isAuthenticated: Auth.isAuthenticated()}, () => {console.log(this.state.isAuthenticated)});
        //checking to see if there is any valid token on the storage and if there is => isAuthenticated = true
    }

    componentDidMount(){
        this.setIsAuthenticated();
    }
    render() {
        const NavBarWithRouter = withRouter(NavBar); //NavBar with props: history, match, location
        Auth.checkToken();
      console.log(this.state.isAuthenticated);
      return (
            <Router>
                <NavBarWithRouter
                    isAuthenticated={this.state.isAuthenticated}
                    onLogout={this.setIsAuthenticated}
                />
                <div className={"container"}>
                <Switch>
                    <Route
                        path="/login"
                        render={(props)=><Login
                        isAuthenticated={this.state.isAuthenticated}
                        onLogin={this.setIsAuthenticated}
                        {...props}
                        />}
                    />

                    <Route path="/register" >
                        <Register />
                    </Route>
                    <Route path="/artists">
                        <ArtistList />
                    </Route>

                    <Route path="/register"
                           render={(props)=><Register
                            {...props}  />}

                    />

                    <Route path="/" >
                        <Search />
                    </Route>

                </Switch>
                    </div>
            </Router>
    );
  }
}

export default App;
