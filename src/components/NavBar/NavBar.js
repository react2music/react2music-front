import React from 'react';
import './NavBar.css';
import {Link} from "react-router-dom";
import Auth from "../../util/Auth";

class NavBar extends React.Component{
    constructor(props) {
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
    }

    handleLogout(){
       Auth.logout();
       this.props.onLogout(false);
       this.props.history.push("/login");
    }
    render() {
        return (
            <>
                <nav className="navbar navbar-expand-lg navbar-dark text-danger sticky-top Pink">
                    <h4><a className="navbar-brand" href="http://localhost:3000/">react<span className="Blue-text">2</span>music</a></h4>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02"
                            aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarColor02">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <Link to="/home" className="nav-link">Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/artists" className="nav-link">Artists</Link>
                            </li>

                            {(!this.props.isAuthenticated && (
                            <>
                            <li className="nav-item">
                                <Link to="/login" className="nav-link">Login</Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/register" className="nav-link">Register</Link>
                            </li>
                            </>
                            )) || (
                                <>
                                <li className="nav-item active">
                                    <Link to="/search" className="nav-link">Search</Link>
                                </li>

                            <li className="nav-item">
                                <button onClick={this.handleLogout} className="nav-link btn">Logout</button>
                            </li>
                                </>
                            )}
                        </ul>

                    </div>
                </nav>
            </>
        );
    }
}

export default NavBar;