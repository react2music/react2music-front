import React from 'react';
import './Track.css';

class Track extends React.Component {
    constructor(props) {
        super(props);
        this.addTrack = this.addTrack.bind(this);
        this.removeTrack = this.removeTrack.bind(this);
        this.playTrack = this.playTrack.bind(this);
        this.pauseTrack = this.pauseTrack.bind(this);
        this.likeTrack = this.likeTrack.bind(this);
        this.unlikeTrack = this.unlikeTrack.bind(this);
    }

    renderAction(){
        if (this.props.isRemoval){
            return <button className="Track-action mx-2" onClick={this.removeTrack}>-</button>
        }else {
            return <button className="Track-action mx-2" onClick={this.addTrack}>+</button>
        }
    }

    renderPlayPause(){
        if (this.props.isPlayable){
            return <button className="Track-action Play" onClick={this.playTrack}></button>
        }else {
            return <button className="Track-action Pause" onClick={this.pauseTrack}></button>
        }
    }

    renderLike(){
        if (this.props.isLiked){
            return <button className="Track-action Like mx-3" onClick={this.likeTrack}></button>
        }else {
            return <button className="Track-action Unlike mx-3" onClick={this.unlikeTrack}></button>
        }
    }

    addTrack(){
        this.props.onAdd(this.props.track);
    }

    removeTrack(){
        this.props.onRemove(this.props.track);
    }

    playTrack(){
        this.props.onPlay(this.props.track);
    }

    pauseTrack(){
        this.props.onPause(this.props.track);
    }

    likeTrack(){
        this.props.onLike(this.props.track);
    }

    unlikeTrack(){
        this.props.onUnlike(this.props.track);
    }



    render() {
        return (
            <div className="Track" key={this.props.track.id}>
                <div className="Track-information">
                    <h3>{this.props.track.name}</h3>
                    <p>{this.props.track.artist} | {this.props.track.duration.substr(14, 5)}  | {this.props.track.bpm} bpm</p>
                </div>
                {this.renderLike()}
                {this.renderPlayPause()}
                {this.renderAction()}
            </div>
        );
    }
}

export default Track;