import React from "react";
import './MusicPlayer.css';

class MusicPlayer extends React.Component{
  /*  constructor(props) {
        super(props);
    }*/
    render() {
        return (
            <audio
                controls autoPlay={true}
                src={this.props.src}>
                Your browser does not support the
                <code>audio</code> element.
            </audio>

        );
    }
}
export default MusicPlayer;

