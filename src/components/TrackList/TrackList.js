import React from 'react';
import './TrackList.css';
import Track from "../Track/Track";

class TrackList extends React.Component {
    render() {
        return (
            <div className="TrackList">

                {/* Pass onRemove and isRemoval from the TrackList component to the Track component
                isRemoval={this.props.isRemoval} !!!! */}
                {
                    this.props.tracks &&
                    this.props.tracks.map((track) => {
                        return <Track track={track}
                                      key={track.id}

                                   onAdd={this.props.onAdd}

                                      onRemove={this.props.onRemove}
                                      isRemoval={this.props.isRemoval}

                                      onPlay={this.props.onPlay}
                                      isPlayable={this.props.isPlayable}

                                      tracks={this.props.tracks}
                        />
                    })
                }
            </div>
        );
    }
}

export default TrackList;