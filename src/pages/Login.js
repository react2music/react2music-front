import React from 'react';
import './Register.css';
import Auth from '../util/Auth';
import { toast } from "react-toastify";

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            inputUsername: '',
            inputPassword: '',
            error: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    /*will do the request for logging in a new user GET /api/users */
    handleSubmit = async e=> {
        e.preventDefault();
        try {
            toast.configure()
            await Auth.authenticate({
                username: `${this.state.inputUsername}`,
                password: `${this.state.inputPassword}`
            });
            this.setState({error: ""});
            this.props.onLogin(true);
            toast.success("You are connected now.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                className: 'Pink'
            });
            //console.log(this.props.history);
            this.props.history.replace("/search");
            //TODO (1): redirect somewhere else
        } catch (error) {
            this.setState({
                error: "Invalid credentials: email doesn't exist or invalid password."
            }, ()=>{console.log(this.state.error)});
            toast.error("There was an error. Try again.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                className: 'bg-dark'
            });
        }
    };

    handleChange(e){
        this.setState({[e.target.id]: e.target.value});
    }

    render() {
        return (
            <div className="jumbotron w-60 mx-auto mt-5 Register">
                <div className="container-fluid">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group">
                            <h1>Login</h1>

                        </div>
                        <div className="form-group">
                            <label htmlFor="inputUsername">Email address:</label>
                            <input
                                type="text"
                                className={"form-control" + (this.state.error && " is-invalid")}
                                id="inputUsername"
                                placeholder="Enter email address"
                                onChange={this.handleChange}
                                required
                               />
                            {this.state.error && <p className="invalid-feedback">{this.state.error}</p>}
                        </div>

                        <div className="form-group">
                            <label htmlFor="inputPassword">Password:</label>
                            <input
                                type="password"
                                className="form-control" id="inputPassword"
                                placeholder="Password"
                                onChange={this.handleChange}
                                required
                            />
                        </div>
                        <button type="submit" className="btn btn-primary float-right Pink">Login</button>
                    </form>
                </div>
            </div>
        )
    }
}
export default Login;