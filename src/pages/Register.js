import React from 'react';
import './Register.css';
import RegisterUser from "../util/RegisterUser";
import { toast } from "react-toastify";

class Register extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            inputUsername: '',
            inputName: '',
            inputSurname: '',
            inputEmail: '',
            inputPassword: '',
            inputBirthdate: '',
            inputPhoto: 'default_avatar.jpg',
            errors: {
                pseudo: '',
                last_name: '',
                name: '',
                email: '',
                password: '',
                birthday: '',
                photo: '',
                roles:''
            }
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e){
        this.setState({[e.target.id]: e.target.value});
    }

    /*will do the request for registering a new user POST /api/users */
    handleSubmit = async e => {
        e.preventDefault();
        const jsonErrors = {};
        console.log(`${this.state.inputUsername} +  ${this.state.inputName} + ${this.state.inputSurname} + ${this.state.inputEmail} +  ${this.state.inputPassword} +  ${this.state.inputBirthdate}`);
        let registerData = {
            email: `${this.state.inputEmail}`,
            password: `${this.state.inputPassword}`,
            roles: ["ROLE_USER"],
            name: `${this.state.inputName}`,
            lastName: `${this.state.inputSurname}`,
            pseudo: `${this.state.inputUsername}`,
            birthday: `${this.state.inputBirthdate}`,
            photo: `${this.state.inputPhoto}`
        }
        console.log(registerData);
        toast.configure();
        try {
            const response = await RegisterUser.register(registerData);
            this.setState({errors: {}});
            console.log(response);
            toast.success("You are registered now.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                className: 'Pink'
            });
            this.props.history.replace("/login");
        }catch (e) {
            console.log(e.response.data);
            const { violations } = e.response.data;
            if (violations) {
                violations.forEach(violation => {
                    jsonErrors[violation.propertyPath] = violation.message;
                });
                this.setState({errors: jsonErrors});
            }
            console.log(jsonErrors);
            toast.error("There was an error with your data.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                className: 'bg-dark'
            });
        }
    }

    render() {
        return (
            <div className="jumbotron w-60 mx-auto mt-5 Register">
                <div className="container-fluid">
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <h1>Register</h1>
                        <p>Please fill in this form to create an account.</p>
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputName">Username:</label>
                        <input
                            type="text"
                            className={"form-control" + (this.state.errors.pseudo && " is-invalid")}
                            id="inputUsername"
                            placeholder="Enter username"
                            onChange={this.handleChange}
                            required
                        />
                        {this.state.errors.pseudo  && <p className="invalid-feedback">{this.state.errors.pseudo }</p>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputName">Name:</label>
                        <input
                            type="text"
                            className={"form-control" + (this.state.errors.name && " is-invalid")}
                            id="inputName"
                            placeholder="Enter name"
                            onChange={this.handleChange}
                            required
                        />
                        {this.state.errors.name && <p className="invalid-feedback">{this.state.errors.name}</p>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputSurname">Lastname:</label>
                        <input type="surname"
                               className={"form-control" + (this.state.errors.last_name && " is-invalid")}
                               id="inputSurname"
                               placeholder="Enter surname"
                               onChange={this.handleChange}
                               required
                        />
                        {this.state.errors.last_name && <p className="invalid-feedback">{this.state.errors.last_name}</p>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputEmail">Email address:</label>
                        <input
                            type="email"
                            className={"form-control" + (this.state.errors.email && " is-invalid")} id="inputEmail"
                            placeholder="Enter email"
                            onChange={this.handleChange}
                            required
                        />
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with
                                anyone else.</small>
                        {this.state.errors.email && <p className="invalid-feedback">{this.state.errors.email}</p>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="inputPassword">Password:</label>
                        <input
                            type="password"
                            className={"form-control" + (this.state.errors.password && " is-invalid")}
                            id="inputPassword"
                            placeholder="Password"
                            onChange={this.handleChange}
                            required/>
                        {this.state.errors.password && <p className="invalid-feedback">{this.state.errors.password}</p>}
                    </div>
                    <div className="form-group">
                        <label className="control-label" htmlFor="inputBirthdate">Date</label>
                        <input
                            type="date"
                            className={"form-control" + (this.state.errors.inputBirthdate && " is-invalid")}
                            id="inputBirthdate"
                            placeholder="yy-mm-dd"
                            onChange={this.handleChange}
                            required
                        />
                        {this.state.errors.inputBirthdate && <p className="invalid-feedback">{this.state.errors.inputBirthdate}</p>}
                    </div>
                    <button type="submit" className="btn btn-primary float-right Pink">Register</button>
                </form>
                    </div>
            </div>
        )
    }
}
export default Register;