import React from 'react';
import SearchBar from '../components/SearchBar/SearchBar';
import React2MusicBackEnd from '../util/React2MusicBackEnd';
import SearchResults from '../components/SearchResults/SearchResults';
import MusicPlayer from '../components/MusicPlayer/MusicPlayer';
import JkMusicPlayer from '../components/JkMusicPLayer/JkMusicPlayer';

class Search extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            searchResults: [
            ],
            showSearchResults: false,
            playingSong: {
                album: "undefined",
                artist: "Luis Fonsi",
                bpm: 90,
                duration: "04:42",
                fileMp3: "http://blackbird.planethoster.world/react2music/Luis-Fonsi-ft.-Daddy-Yankee--Despacito.mp3",
                id: 6,
                name: "Despacito"
            },
            audioList: [
                {
                    name: 'Esti',
                    singer: 'The Krypronite Sparks',
                    cover:
                        'http://blackbird.planethoster.world/react2music/covers/esti.jpg',
                    musicSrc: () => {
                        return Promise.resolve(
                            'http://blackbird.planethoster.world/react2music/The_Kryptonite_Sparks_-_Esti.mp3'
                        )
                    }
                },
                {
                    name: 'Despacito',
                    singer: 'Luis Fonsi',
                    cover: 'http://blackbird.planethoster.world/react2music/covers/despacito.png',
                    musicSrc: 'http://blackbird.planethoster.world/react2music/Luis-Fonsi-ft.-Daddy-Yankee--Despacito.mp3',
                    //lyric
                },
                {
                    name: 'Bella, Ciao',
                    singer: 'Manu Chao',
                    cover:
                        'http://blackbird.planethoster.world/react2music/covers/bellaciao.jpg',
                    musicSrc: () => {
                        return Promise.resolve(
                            'http://blackbird.planethoster.world/react2music/Manu_Chao_-_Bella_Ciao.mp3'
                        )
                    }
                },
                {
                    name: 'Un sfert de secundă',
                    singer: 'The Mono Jacks',
                    cover:
                        'http://blackbird.planethoster.world/react2music/covers/secunda.jpg',
                    musicSrc: () => {
                        return Promise.resolve(
                            'http://blackbird.planethoster.world/react2music/The_Mono_Jacks_-_Un_sfert_de_secunda.mp3'
                        )
                    }
                }
            ]
        };
        this.search = this.search.bind(this);
        this.addTrack = this.addTrack.bind(this);
        this.removeTrack = this.removeTrack.bind(this);
        this.playTrack = this.playTrack.bind(this);
        this.renderAutoPlay = this.renderAutoPlay.bind(this);
    }

    search(term){  //+ passing search() to the SearchBar component as an onSearch attribute
        console.log(`will look after ${term}`);
        React2MusicBackEnd.search(term).then(searchResults => {
            console.log("updating search results playlist");
            console.log(searchResults);
            this.setState({searchResults: searchResults});
            this.setState({showSearchResults: true})
        })
    }

    addTrack(track){
        console.log('add to playlist');
    }
    removeTrack(track) {
        console.log('remove from playlist');
    }

    playTrack(track){
        console.log("will play song");
        this.setState({playingSong: track}, ()=>{
            console.log(this.state.playingSong);

        });

        this.setState({
            audioList: [
                {
                    name: track.name,
                    singer: track.artist,
                    cover:
                        'http://blackbird.planethoster.world/react2music/covers/esti.jpg',
                    musicSrc: () => {
                        return Promise.resolve(
                            track.fileMp3
                        )
                    }
                }
        ]
        }, ()=>{console.log(this.state.audioList);});


        //https://stackoverflow.com/a/41446620
        /*
         setState() is usually asynchronous, which means that at the time you console.log
         the state, it's not updated yet. Try putting the log in the callback of the setState()
         method. It is executed after the state change is complete
        * */
    }

    //if there is a song in the state.playingSong, the <MusicPlayer> will get the attribute 'autoplay'
    renderAutoPlay() {
        if (this.state.playingSong) {
            return 'autoplay';
        } else {
            return '';
        }
    }
    render() {
        return (
            <div>
                <SearchBar onSearch={this.search}/>
                <div className="App-playlist">
                    {this.state.showSearchResults &&
                    <SearchResults searchResults={this.state.searchResults}
                                   onAdd={this.addTrack} onPlay={this.playTrack}
                    />}
                </div>
                <JkMusicPlayer audioList={this.state.audioList}/>
                {/*   <MusicPlayer src={`${this.state.playingSong.fileMp3}`}  /> */}
            </div>
        );
    }
}

export default Search;